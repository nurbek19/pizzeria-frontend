import React from 'react';
import './Form.css';

const Form = props => {
  return(
      <form action="#" className="form">
          <legend>{props.pageTitle}</legend>

          <label>
              <input
                  type="text"
                  placeholder="Add title"
                  name="title"
                  value={props.title}
                  onChange={props.changeValue}
              />
          </label>
          <label>
              <input
                  type="number"
                  placeholder="Add price"
                  name="price"
                  value={props.price}
                  onChange={props.changeValue}
              />
          </label>
          <label>
              <input
                  type="text"
                  placeholder="Add image url"
                  name="image"
                  value={props.image}
                  onChange={props.changeValue}
              />
          </label>

          <button onClick={props.saveDishes}>Save</button>
      </form>
  )
};

export default Form;
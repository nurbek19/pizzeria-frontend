import React from 'react';
import './DishesItem.css';

const DishesItem = props => {
    return (
        <div className="dishes-item">
            <div className="dishes-description">
                <img src={props.img} alt="pizza"/>
                <h4>{props.title}</h4>
            </div>

            <div className="dishes-actions">
                <p>{props.price}</p>
                <button onClick={props.edit}>Edit</button>
                <button onClick={props.removeItem}>Delete</button>
            </div>
        </div>
    )
};

export default DishesItem;
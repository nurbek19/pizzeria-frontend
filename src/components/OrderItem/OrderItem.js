import React from 'react';
import './OrderItem.css';

const OrderItem = props => {
    return (
        <div className="order-item">
            <div className="order-list">
                {props.items}
                <p>Delivery <b>150 KGS</b></p>
            </div>

            <div className="total">
                <p>Order total: <b>{props.total} KGS</b></p>
                <button onClick={props.removeOrder}>Complete order</button>
            </div>
        </div>
    )
};

export default OrderItem;
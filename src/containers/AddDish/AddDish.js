import React, {Component} from 'react';
import {connect} from 'react-redux';

import Form from '../../components/Form/Form';
import './AddDish.css';
import {addDishes} from "../../store/actions/dishes";

class AddDish extends Component {

    state = {
        title: '',
        price: '',
        image: ''
    };

    changeValue = event => {
        const name = event.target.name;
        this.setState({
            [name]: event.target.value
        })
    };

    addFoods = event => {
        event.preventDefault();
        const dishes = {...this.state};
        console.log(dishes);
        this.props.addDishes(dishes, this.props.history);
    };

    render() {
        let form = <Form
            pageTitle="Add dish"
            title={this.state.title}
            price={this.state.price}
            image={this.state.image}
            changeValue={this.changeValue}
            saveDishes={this.addFoods}
        />;

        if(this.props.loading) {
            form = <p>Loading...</p>;
        }

        return (
            <div className="add-dish">
                {form}
            </div>
        )
    }
}

const mapToProps = state => {
    return {
        loading: state.dishes.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addDishes: (dishes, history) => dispatch(addDishes(dishes, history))
    }
};

export default connect(mapToProps, mapDispatchToProps)(AddDish);
import React, {Component} from 'react';
import {connect} from 'react-redux';

import OrderItem from '../../components/OrderItem/OrderItem';
import './Orders.css';
import {getOrders, removeOrder} from "../../store/actions/orders";
import {getDishes} from "../../store/actions/dishes";

class Orders extends Component {
    componentDidMount() {
        this.props.getOrders();
        this.props.getDishes();
    }

    render() {
        let orders = (this.props.dishes && this.props.orders ?
            Object.keys(this.props.orders).map(orderKey => {
                const order = this.props.orders[orderKey];
                return <OrderItem key={orderKey}
                                  items={Object.keys(order).map((dishKey, index) => {
                                      const dish = this.props.dishes[dishKey];
                                      return <p key={index}>{dish.title} x{order[dishKey]} <b>{dish.price} KGS</b></p>
                                  })}

                                  total={
                                      Object.keys(order).map(dishKey => {
                                          const dish = this.props.dishes[dishKey];
                                          let total = 0;
                                          total = total + order[dishKey] * parseInt(dish.price, 10);

                                          return total;
                                      }).reduce((acc, item) => acc + item, 150)}
                                  removeOrder={() => this.props.removeOrder(orderKey)}
                />
            }) : null);

        if(this.props.loading) {
            orders = <p style={{fontSize: '60px', textAlign: 'center'}}>Loading..</p>
        }

        return (
            <div className="orders">
                {orders}
            </div>
        );
    }
}

const mapToProps = state => {
    return {
        orders: state.orders.orders,
        dishes: state.dishes.dishes,
        loading: state.orders.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getOrders: () => dispatch(getOrders()),
        getDishes: () => dispatch(getDishes()),
        removeOrder: key => dispatch(removeOrder(key))
    }
};

export default connect(mapToProps, mapDispatchToProps)(Orders);
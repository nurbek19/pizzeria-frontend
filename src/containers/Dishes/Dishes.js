import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import './Dishes.css';
import {getDishes, removeDishes} from "../../store/actions/dishes";
import DishesItem from '../../components/DishesItem/DishesItem';

class Dishes extends Component {
    componentDidMount() {
        this.props.getDishes();
    }

    purchaseEditHandler = id => {
        this.props.history.push(`/edit-dish/${id}`);
    };

    render() {
        let dishes = (this.props.dishes ? Object.keys(this.props.dishes).map((dishKey, index) => {
            const dish = this.props.dishes[dishKey];

            return <DishesItem
                key={index}
                img={dish.image}
                title={dish.title}
                price={dish.price}
                edit={() => this.purchaseEditHandler(dishKey)}
                removeItem={() => this.props.removeDishes(dishKey)}
            />
        }): null);

        if(this.props.show) {
            dishes = <p>Loading...</p>;
        }

        return (
            <div className="dishes">
                <div className="title-block">
                    <h2>Dishes</h2>
                    <Link to="/add-dish">Add new Dish</Link>
                </div>

                <div className="dishes-list">
                    {dishes}
                </div>
            </div>
        )
    }
}

const mapToProps = state => {
    return {
        dishes: state.dishes.dishes,
        show: state.dishes.show
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getDishes: () => dispatch(getDishes()),
        removeDishes: (id) => dispatch(removeDishes(id))
    }
};

export default connect(mapToProps, mapDispatchToProps)(Dishes);
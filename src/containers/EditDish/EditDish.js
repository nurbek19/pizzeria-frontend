import React, {Component} from 'react';
import {connect} from 'react-redux';

import Form from '../../components/Form/Form';
import {getEditedDish, editDish} from "../../store/actions/dishes";

class EditDish extends Component {

    state = {
        title: '',
        price: '',
        image: ''
    };

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.getEditedDish(id);
    }

    componentWillReceiveProps(nextProps) {
        const choosenDish = {...nextProps.editedDish};
        this.setState({
            title: choosenDish.title,
            price: choosenDish.price,
            image: choosenDish.image
        });
    }

    changeValue = event => {
        const name = event.target.name;
        this.setState({
            [name]: event.target.value
        })
    };

    editDishHandler = event => {
        event.preventDefault();
        const id = this.props.match.params.id;
        const dishes = {...this.state};

        this.props.editDish(id, dishes, this.props.history);
    };

    render() {
        let form = <Form
            pageTitle="Edit dish"
            changeValue={this.changeValue}
            title={this.state.title}
            price={this.state.price}
            image={this.state.image}
            saveDishes={this.editDishHandler}
        />;

        if(this.props.loading) {
            form = <p>Loading...</p>;
        }

        return (
            <div className="edit-dish">
                {form}
            </div>
        )
    }
}

const mapToProps = state => {
    return {
        loading: state.dishes.loading,
        editedDish: state.dishes.editedDish
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getEditedDish: id => dispatch(getEditedDish(id)),
        editDish: (id, dish, history) => dispatch(editDish(id, dish, history))
    }
};

export default connect(mapToProps, mapDispatchToProps)(EditDish);
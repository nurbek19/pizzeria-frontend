import React, {Component, Fragment} from 'react';
import {Route, Switch, NavLink, Link} from 'react-router-dom';

import Dishes from "./containers/Dishes/Dishes";
import AddDish from "./containers/AddDish/AddDish";
import Orders from "./containers/Orders/Orders";
import EditDish from "./containers/EditDish/EditDish";

class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Link to="/">Papa Pizza Admin</Link>

                    <ul>
                        <li>
                            <NavLink to="/dishes">Dishes</NavLink>
                        </li>
                        <li>
                            <NavLink to="/orders">Orders</NavLink>
                        </li>
                    </ul>
                </header>
                <Switch>
                    <Route path="/" exact component={Dishes}/>
                    <Route path="/dishes" component={Dishes}/>
                    <Route path="/add-dish" component={AddDish}/>
                    <Route path="/edit-dish/:id" component={EditDish}/>
                    <Route path="/orders" component={Orders}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;

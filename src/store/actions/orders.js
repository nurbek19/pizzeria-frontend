import axios from '../../axios-dishes';
import * as actionTypes from './actionTypes';

export const orderRequest = () => {
  return {type: actionTypes.ORDERS_REQUEST}
};

export const orderSuccess = orders => {
  return {type: actionTypes.ORDERS_SUCCESS, orders}
};

export const ordersError = () => {
  return {type: actionTypes.ORDERS_ERROR}
};

export const removeOrderRequest = () => {
  return {type: actionTypes.REMOVE_ORDER_REQUEST}
};

export const removeOrderSuccess = key => {
  return {type: actionTypes.REMOVE_ORDER_SUCCESS, key}
};

export const removeOrderError = () => {
  return {type: actionTypes.REMOVE_ORDER_ERROR}
};

export const getOrders = () => {
    return dispatch => {
        dispatch(orderRequest());
        axios.get('/dishes-orders.json').then(response => {
            dispatch(orderSuccess(response.data));
        }, error => {
            dispatch(ordersError());
        })
    }
};

export const removeOrder = key => {
    return dispatch => {
        dispatch(removeOrderRequest());
        axios.delete(`/dishes-orders/${key}.json`).then(() => {
            dispatch(removeOrderSuccess(key));
        }, error => {
            dispatch(removeOrderError());
        })
    }
};
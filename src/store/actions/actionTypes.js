export const DISHES_REQUEST = 'DISHES_REQUEST';
export const DISHES_SUCCESS = 'DISHES_SUCCESS';
export const DISHES_ERROR = 'DISHES_ERROR';

export const LIST_DISHES_REQUEST = 'LIST_DISHES_REQUEST';
export const LIST_DISHES_SUCCESS = 'LIST_DISHES_SUCCESS';
export const LIST_DISHES_ERROR = 'LIST_DISHES_ERROR';

export const REMOVE_DISHES_REQUEST = 'REMOVE_DISHES_REQUEST';
export const REMOVE_DISHES_SUCCESS = 'REMOVE_DISHES_SUCCESS';
export const REMOVE_DISHES_ERROR = 'REMOVE_DISHES_ERROR';

export const ORDERS_REQUEST = 'ORDERS_REQUEST';
export const ORDERS_SUCCESS = 'ORDERS_SUCCESS';
export const ORDERS_ERROR = 'ORDERS_ERROR';

export const EDITED_DISH_REQUEST = 'EDITED_DISH_REQUEST';
export const EDITED_DISH_SUCCESS = 'EDITED_DISH_SUCCESS';
export const EDITED_DISH_ERROR = 'EDITED_DISH_ERROR';

export const EDIT_DISH_REQUEST = 'EDIT_DISH_REQUEST';
export const EDIT_DISH_SUCCESS = 'EDIT_DISH_SUCCESS';
export const EDIT_DISH_ERROR = 'EDIT_DISH_ERROR';

export const REMOVE_ORDER_REQUEST = 'REMOVE_ORDER_REQUEST';
export const REMOVE_ORDER_SUCCESS = 'REMOVE_ORDER_SUCCESS';
export const REMOVE_ORDER_ERROR = 'REMOVE_ORDER_ERROR';
import axios from '../../axios-dishes';
import * as actionTypes from './actionTypes';

export const dishRequest = () => {
    return {type: actionTypes.DISHES_REQUEST}
};

export const dishSuccess = () => {
    return {type: actionTypes.DISHES_SUCCESS}
};

export const dishError = () => {
    return {type: actionTypes.DISHES_ERROR}
};

export const listDishesRequest = () => {
    return {type: actionTypes.LIST_DISHES_REQUEST}
};

export const listDishesSuccess = (dishes) => {
    return {type: actionTypes.LIST_DISHES_SUCCESS, dishes}
};

export const listDishesError = () => {
    return {type: actionTypes.LIST_DISHES_ERROR}
};

export const removeDishesRequest = () => {
    return {type: actionTypes.REMOVE_DISHES_REQUEST}
};

export const removeDishesSuccess = (id) => {
    return {type: actionTypes.REMOVE_DISHES_SUCCESS, id}
};

export const removeDishesError = () => {
    return {type: actionTypes.REMOVE_DISHES_ERROR}
};

export const editedDishRequest = () => {
    return {type: actionTypes.EDITED_DISH_REQUEST}
};

export const editedDishSuccess = editedDish => {
    return {type: actionTypes.EDITED_DISH_SUCCESS, editedDish}
};

export const editedDishError = () => {
    return {type: actionTypes.EDITED_DISH_ERROR}
};

export const editDishRequest = () => {
    return {type: actionTypes.EDIT_DISH_REQUEST}
};

export const editDishSuccess = () => {
    return {type: actionTypes.EDIT_DISH_SUCCESS}
};

export const editDishError = () => {
    return {type: actionTypes.EDIT_DISH_ERROR}
};

export const addDishes = (dishes, history) => {
    return dispatch => {
        dispatch(dishRequest());
        axios.post('/foods.json', dishes).then(() => {
            dispatch(dishSuccess());
            history.push('/');
        }, error => {
            dispatch(dishError(error));
        })
    }
};

export const getDishes = () => {
    return dispatch => {
        dispatch(listDishesRequest());
        axios.get('/foods.json').then(response => {
            dispatch(listDishesSuccess(response.data));
        }, error => {
            dispatch(listDishesError());
        })
    }
};

export const removeDishes = id => {
    return dispatch => {
        dispatch(removeDishesRequest());
        axios.delete(`/foods/${id}.json`).then(response => {
            dispatch(removeDishesSuccess(id));
        }, error => {
            dispatch(removeDishesError());
        })
    }
};

export const getEditedDish = id => {
    return dispatch => {
        dispatch(editedDishRequest());
        axios.get(`/foods/${id}.json`).then(response => {
            dispatch(editedDishSuccess(response.data));
        }, error => {
            dispatch(editedDishError());
        })
    }
};

export const editDish = (id, dish, history) => {
    return dispatch => {
        dispatch(editDishRequest());
        axios.put(`/foods/${id}.json`, dish).then(() => {
            dispatch(editDishSuccess());
            history.push('/');
        }, error => {
            dispatch(editDishError());
        })
    }
};
import * as actionTypes from '../actions/actionTypes';

const initialState = {
    dishes: null,
    loading: false,
    show: false,
    editedDish: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.DISHES_REQUEST:
            return {...state, loading: true};
        case actionTypes.DISHES_SUCCESS:
            return {...state, loading: false};
        case actionTypes.DISHES_ERROR:
            return {...state, loading: false};
        case actionTypes.LIST_DISHES_REQUEST:
            return {...state, show: true};
        case actionTypes.LIST_DISHES_SUCCESS:
            return {...state, dishes: action.dishes, show: false};
        case actionTypes.LIST_DISHES_ERROR:
            return {...state, show: false};
        case actionTypes.REMOVE_DISHES_REQUEST:
            return {...state, show: true};
        case actionTypes.REMOVE_DISHES_SUCCESS:
            let listDishes = {...state.dishes};
            delete listDishes[action.id];
            JSON.parse(JSON.stringify(listDishes));
            return {...state, dishes: listDishes, show: false};
        case actionTypes.REMOVE_DISHES_ERROR:
            return {...state, show: false};
        case actionTypes.EDITED_DISH_REQUEST:
            return {...state, loading: true};
        case actionTypes.EDITED_DISH_SUCCESS:
            return {...state, editedDish: action.editedDish, loading: false};
        case actionTypes.EDITED_DISH_ERROR:
            return {...state, loading: false};
        case actionTypes.EDIT_DISH_REQUEST:
            return {...state, loading: true};
        case actionTypes.EDIT_DISH_SUCCESS:
            return {...state, loading: false};
        case actionTypes.EDIT_DISH_ERROR:
            return {...state, loading: false};
        default:
            return state;
    }
};

export default reducer;


import * as actionTypes from '../actions/actionTypes';

const initialState = {
    orders: null,
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ORDERS_REQUEST:
            return {...state, loading: true};
        case actionTypes.ORDERS_SUCCESS:

            return {
                ...state,
                orders: action.orders,
                loading: false
            };
        case actionTypes.ORDERS_ERROR:
            return {...state, loading: false};
        case actionTypes.REMOVE_ORDER_REQUEST:
            return {...state, loading: true};
        case actionTypes.REMOVE_ORDER_SUCCESS:
            const orders = {...state.orders};
            delete orders[action.key];

            JSON.parse(JSON.stringify(orders));

            return {
                ...state,
                orders,
                loading: false
            };
        case actionTypes.REMOVE_ORDER_ERROR:
            return {...state, loading: false};
        default:
            return state;
    }
};

export default reducer;